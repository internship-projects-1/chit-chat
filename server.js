const express = require("express");
const http = require("http");
const { Server } = require("socket.io");
const messageFormat = require("./utils/messageFormat.js");
const {
  userJoin,
  getCurrentUser,
  userLeave,
  getRoomUsers,
} = require("./utils/users.js");

const PORT = 3000 || process.env.port;

const app = express();
const server = http.createServer(app);
const io = new Server(server);

const botName = "Chit-Chat Bot";

app.use(express.static(__dirname + "/public"));

// Run when an client connects
io.on("connection", (socket) => {
  socket.on("joinRoom", ({ username, room }) => {
    const user = userJoin(socket.id, username, room);

    socket.join(user.room);

    // Welcome the current user
    socket.emit(
      "message",
      messageFormat(botName, `Welcome to chit-chat ${user.username}`)
    );

    // Runs when a new user connects
    socket.broadcast
      .to(user.room)
      .emit(
        "message",
        messageFormat(botName, `${user.username} has joined the chat`)
      );

      // Send Roominfo to Frontend
      io.to(user.room).emit('roomUsers', {
        room: user.room,
        users: getRoomUsers(user.room)
      })
  });

  // Listen for chatMessage
  socket.on("chatMessage", (msg) => {
    const user = getCurrentUser(socket.id);

    io.to(user.room).emit("message", messageFormat(user?.username, msg));
  });

  // Runs when a user leaves
  socket.on("disconnect", () => {
    const user = userLeave(socket.id);

    if (user) {
      io.to(user.room).emit(
        "message",
        messageFormat(botName, `${user.username} has left the chatroom!!!`)
      );

      // Send Roominfo to Frontend
      io.to(user.room).emit('roomUsers', {
        room: user.room,
        users: getRoomUsers(user.room)
      })
    }
  });
});

server.listen(PORT, () => {
  console.log(`Server up and running on port : ${PORT}`);
});
